# Minidit

Minidit is a simple social news app that lets you share links, comment, and vote on them very similar to reddit.

Minidit itself is free and open-source, and is a good example of common Meteor app patterns such as:

- Routing
- User Accounts
- Notifications
- Errors
- Publications/Subscriptions
- Permissions
